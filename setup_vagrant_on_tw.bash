#!/usr/bin/env bash
# Copyright (c) 2018 Drew Adams <druonysus@opensuse.org>

##
## FUNCTIONS
#######

initialize() {
    # Set RETVAL to zero to start with
    RETVAL=0

    # load in /etc/os-release so we know what we are working with
    if [[ -f /etc/os-release ]]; then
        source /etc/os-release
    else
        ID="UNKNOWN"
        PRETTY_NAME="$ID"
    fi
    
    # check to be sure we are running as root
    if [[ $UID != 0 ]]; then
        print_error hard "Only root can run this script."
    fi

    if [[ $ID != 'opensuse-tumbleweed' ]]; then
        print_error hard \
            "This script is for openSUSE Tumbleweed only! Your system is detected as $PRETTY_NAME. Exiting!"
    fi
}

increment_retval() {
    # ONLY execute this in an error condition. Errors printed with
    # print_error will use the RETVAL as the exit code for this script.
    if [[ $RETVAL == 0 ]]; then
        RETVAL=2
    else
        let RETVAL+=$RETVAL
    fi
}

print_info() {
    # Simple way to echo with an "** INFO:" prefix without having do
    # write that every time.
    local words="$1"
    local endln=${2:-'\n'}
    printf "** INFO: $words${endln}"
}


clean_close() {
    exit $RETVAL
}

print_error() {
    # Simple function to echo to STDERR with an "** ERROR:" prefix without
    # having to write write the redirect and the ERROR header every time.
    # Likely this is the only place where we will want to use either
    # clean_close or increment_retval.
    local error_type=$1
    local words="$2"
    local endln=${3:-'\n'}
    local cmd="printf"
    local header="** ERROR:"
    increment_retval
    case "$error_type" in
        soft)
            >&2 $cmd "$header $words${endln}"
            ;;
        hard)
            >&2 $cmd "$header $words${endln}"
            clean_close
            ;;
        *)
            >&2 $cmd "$header UKNOWN ERROR TYPE. EXITING!${endln}"
            clean_close
            ;;
    esac
}

install_pkg() {
    local pkgs="$@"
    zypper \
        --no-gpg-checks \
        --non-interactive \
        in \
        --oldpackage \
        --force \
        "$pkgs"
}

main() {
    initialize

    PKG=vagrant
    # Vagrant version 1.8.7 from upstream is the last one known to work. All others require much more effort.
    PKG_VER=1.8.7
    PKG_W_VER=$PKG-$PKG_VER
    PKG_URL="https://releases.hashicorp.com/${PKG}/${PKG_VER}/${PKG}_${PKG_VER}_$(arch).rpm"
    MODIFY_FILE="/opt/vagrant/embedded/gems/gems/${PKG_W_VER}/lib/vagrant/shared_helpers.rb"
    FIND='DEFAULT_SERVER_URL = "https://atlas.hashicorp.com"'
    REPLACE='DEFAULT_SERVER_URL = "https://app.vagrantup.com"'

    # If vagrant is already installed, lets just error for now.
    # Maybe it would be good to ask what the user would like to do,
    # but I don't really want this script to be interactive by defult.
    # So if I add that, then I would have to add a lot of other code
    # just for defualts. Not worth it for now.
    print_info "Installing $PKG_W_VER"
    if rpm -q --quiet "$PKG_W_VER"; then
            print_info "Good news! $PKG-$PKG_VER is already be installed."
    else 
        print_info "Installing $PKG_W_VER from $PKG_URL"
        install_pkg "$PKG_URL"
    fi

    print_info "Locking $PKG package."
    if zypper ll | grep -q $PKG; then
        print_info "Good news! $PKG is already locked."
    else
        if zypper al $PKG; then
            print_info "$PKG is now locked."
        else
            print_error hard "$PKG was not able to be locked. Exiting!"
        fi
    fi

    if grep -q "$FIND" $MODIFY_FILE ; then
        print_info "Modifying ${MODIFY_FILE}.\n\tReplacing:\n\t'$FIND'\n\twith:\n\t'$REPLACE'"
        sed -i -e "s#$FIND#$REPLACE#g" $MODIFY_FILE
    fi
}

##
## MAIN EXECUTION
#######
main $@